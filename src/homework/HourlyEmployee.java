package homework;

public class HourlyEmployee extends StaffMember {
    private int hoursWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Employee ID: " + id + "\nEmployee Name: " + name + "\nEmployee Address: " + address + "\nEmployee Hours Worked: " + hoursWorked + "\nEmployee Rate: " + rate + "\nEmployee Payment: " + (hoursWorked * rate);
    }

    @Override
    public double pay() {
        return 0;
    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}

