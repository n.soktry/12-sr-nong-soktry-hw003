package homework;

public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Employee ID: " + id + "\nEmployee Name: " + name + "\nEmployee Address: " + address + "\nEmployee Salary: " + salary + "\nEmployee Bonus: " + bonus + "\nEmployee Payment: " + (salary + bonus);
    }

    @Override
    public double pay() {
        return 0;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
